const joi = require("joi");
const joiObjectId = require("@wegolook/joi-objectid");
const myJoi = joi.extend(joiObjectId);
const mongoose = require("mongoose");
const Schema = mongoose.Schema;
const { Permission } = require("./permission");
const { View } = require("./view");
/*create model of role with relation one to many between role
  and permission and view models
*/
const Role = mongoose.model(
  "qpix12.02.01-Role",
  new mongoose.Schema({
    name: {
      type: String,
      required: true,
      trim: true,
      minlength: 2,
      maxlength: 40
    },
    permissions: [
      { type: Schema.Types.ObjectId, ref: "Permission", required: true }
    ],
    views: [{ type: Schema.Types.ObjectId, ref: "View", required: true }]
  })
);
//validation by joi module
function ValidateRole(role) {
  const schema = {
    name: joi
      .string()
      .min(2)
      .max(40)
      .required(),
    permissionId: myJoi.objectId().required(),
    viewId: myJoi.objectId().required()
  };
  return joi.validate(role, schema);
}

exports.ValidateRole = ValidateRole;
exports.Role = Role;
