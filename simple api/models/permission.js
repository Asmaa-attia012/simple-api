// 1-validation :joi
//2-schema:class
const joi = require("joi");
const mongoose = require("mongoose");

const PermissionSchema = new mongoose.Schema({
  name: {
    type: String,
    required: true,
    minlength: 3,
    maxlength: 20
  },
  IsGlobal: {
    type: Boolean,
    default: false
  }
});
const Permission = mongoose.model("qpix12.02.03-Action", PermissionSchema);

//validation by joi module
function ValidatePermission(permission) {
  const schema = {
    name: joi
      .string()
      .min(4)
      .max(20)
      .required(),
    IsGlobal: joi
      .boolean()
      .required()
      .default(false)
  };
  return joi.validate(permission, schema);
}

exports.Permission = Permission;
exports.PermissionSchema = PermissionSchema;
exports.ValidatePermission = ValidatePermission;
