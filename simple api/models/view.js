const joi = require("joi");
const mongoose = require("mongoose");
const Schema = mongoose.Schema;
const joiObjectId = require("@wegolook/joi-objectid");
const myJoi = joi.extend(joiObjectId);

const { PermissionSchema, Permission } = require("./permission");

const View = mongoose.model(
  "qpix12.02.01-View",
  new mongoose.Schema({
    name: {
      type: String,
      required: true,
      trim: true,
      minlength: 2,
      maxlength: 20
    },
    numberInStock: {
      type: Number
    },
    // relation one :one between permission(action) and view
    // permission: {
    //   type: PermissionSchema,
    //   required: true
    // }
    // another way for relation
    permission: {
      type: Schema.Types.ObjectId,
      ref: "Permission",
      required: true
    }
  })
);

//validation by joi module on schema
function ValidateView(view) {
  const schema = {
    name: joi
      .string()
      .min(3)
      .max(333)
      .required(),
    numberInStock: joi.number().required(),
    permissionId: myJoi.objectId().required()
  };
  return joi.validate(view, schema);
}

exports.ValidateView = ValidateView;
exports.View = View;
