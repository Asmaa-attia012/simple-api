//import of builtIn modules
const mongoose = require("mongoose");
const express = require("express");
const router = express.Router();
const fawn = require("fawn");
fawn.init(mongoose); //to treat with db
//import of created models
const { Permission, PermissionSchema } = require("../models/permission");
const { View } = require("../models/view");
const { Role, ValidateRole } = require("../models/role");

//routers
//1-get all list sort by name
router.get("/", async (req, res) => {
  const roles = await Role.find().sort("name");
  res.send(roles);
});

//2-get by id
router.get("/:id", async (req, res) => {
  const role = await Role.findById(req.params.id);
  if (!role) return res.status(404).send(" The role is not found");
  res.send(role);
});
//3-create new roles
router.post("/", async (req, res) => {
  //first step always validate body based on its schema
  const { error } = ValidateRole(req.body);
  if (error) return res.status(400).send(error.details[0].message);

  //find permission by id plus check if there is permission or not
  const permission = await Permission.findOne(req.params.permissionId);
  if (!permission) return res.status(400).send("invalid permission.");

  //find view by id plus check if there is view to relate with role or not
  const view = await View.findOne(req.params.viewId);
  if (!view) return res.status(400).send("invalid view.");

  //check on number in stock before post check exist thing to buy or not
  if (view.numberInStock === 0)
    return res.status(400).send("the view is not in stock");

  //if(!IsGlobal){
  let role = new Role({
    permission: {
      _id: permission.permissionId,
      name: permission.name
    },
    view: {
      _id: view._id,
      name: view.name
    }
  });
  // else
  // {
  //    res.send('To add role you should have view and permission')
  //  };

  /*transaction which called in mongodb two phase commits by fawn module
 as here i have two operation save then update when i do save firstly ,
  will create temporary document before set in origin doc. rental ,
  then will go to next transaction update , check first then update and delete temp. doc. */
  try {
    new fawn.Task()
      .save("roles", role)
      .update("views", { _id: view._id }, { $inc: { numberInStock: -1 } }) //increment
      .run();
    res.send(role);
  } catch (ex) {
    res.status(500).send("something failed..");
  }
});
//edit role by given id
router.put("/:id", async (req, res) => {
  const { error } = ValidateRole(req.body);
  if (error) return res.status(400).send(error.details[0].message);
  //handle  existed permission
  const PermissionSelected = await View.findById(req.params.id);
  if (!PermissionSelected) return res.status(400).send("invalid permission");
 //handle existed view
  const ViewSelected = await View.findById(req.params.viewId);
  if (!ViewSelected) return res.status(400).send("invalid entered view");
  //findByIdAndUpdate :return object and update directly to db
  const UpdatedRole = await Role.findOneAndUpdate(
    req.params.id,
    {
      name: req.body.name,
      //embedded object relation
      permission: {
        _id: PermissionSelected._id,
        name: PermissionSelected.name
      },
       view: {
        _id: ViewSelected._id,
        name: ViewSelected.name
      }
    },
    { new: true }
  );
  if (!UpdatedRole) return res.status(404).send("Role With Given Id Is Not found");
  res.send(UpdatedRole);
});

//4-delete role with given id
router.delete("/:id", async (req, res) => {
  const role = await Role.findOneAndRemove(req.params.id);
  if (!role) return res.status(404).send("Role With Given Id Is Not found");
  res.send(role);
});

module.exports = router;
