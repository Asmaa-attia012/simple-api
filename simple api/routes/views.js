const mongoose = require("mongoose");
const express = require("express");
const router = express.Router();
const { View, ValidateView } = require("../models/view");
const { Permission, PermissionSchema } = require("../models/permission");

//routers
//1-get all list sort by name
router.get("/", async (req, res) => {
  const views = await View.find().sort("name");
  res.send(views);
});
//2-create post
router.post(
  "/",
  async (req, res) => {
    //first step always validate body based on its schema
    const { error } = ValidateView(req.body);
    if (error) return res.status(400).send(error.details[0].message);
    //find by id then checked if existed or not
    const selectedPermission = await Permission.findOne(
      req.params.permissionId
    );
    // if (!selectedPermission) return res.status(400).send("you entered invalid permission");
    // if (IsGlobal) {
    const view = new View({
        name: req.body.name,
        //embedded object relation
        permission: {
          _id: selectedPermission._id,
          name: selectedPermission.name
        },
        numberInStock: req.body.numberInStock
      }),
      ViewSaved = await view.save();
    res.send(ViewSaved);
  }
  //   else
  //     res.send("You have no permission to create view");
  //
);
//3-update
router.put("/:id", async (req, res) => {
  const { error } = ValidateView(req.body);
  if (error) return res.status(400).send(error.details[0].message);
  //handle  existed
  const PermissionSelected = await View.findById(req.params.id);
  if (!PermissionSelected) return res.status(400).send("invalid permission");
  //findByIdAndUpdate :return object and update directly to db
  const UpdatedView = await View.findOneAndUpdate(
    req.params.id,
    {
      name: req.body.name,
      numberInStock: req.body.numberInStock,
      //embedded object relation
      permission: {
        _id: PermissionSelected._id,
        name: PermissionSelected.name
      }
    },
    { new: true }
  );
  if (!UpdatedView)
    return res.status(404).send("View With Given Id Is Not found");
  res.send(UpdatedView);
});

//4-delete
router.delete("/:id", async (req, res) => {
  const view = await View.findOneAndRemove(req.params.id);
  if (!view) return res.status(404).send("View With Given Id Is Not found");
  res.send(view);
});

//5-get by id
router.get("/:id", async (req, res) => {
  const view = await View.findById(req.params.id);
  if (!view) return res.status(404).send(" View With Given Id Is Not found");
  res.send(view);
});

module.exports = router;
