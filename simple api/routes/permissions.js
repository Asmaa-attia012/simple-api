const mongoose = require("mongoose");
const express = require("express");
const router = express.Router();
const { Permission ,ValidatePermission} = require("../models/permission");

//routers
//1-get all permissions list sort by name
router.get("/", async (req, res) => {
  const permissions = await Permission.find().sort("name");
  res.send(permissions);
});
//2-create
router.post("/", async (req, res) => {
  const { error } = ValidatePermission(req.body);
  if (error) return res.status(400).send(error.details[0].message);
  let permission = new Permission({
      name: req.body.name,
      IsGlobal: req.body.IsGlobal
    }),//???
    permissionAdded = await permission.save();
  res.send(permissionAdded);
});

//3-update
router.put("/:id", async (req, res) => {
  const { error } = ValidatePermission(req.body);
  if (error) return res.status(400).send(error.details[0].message);
  let permission = await Permission.findOneAndUpdate(
    req.params.id,
    {
      name: req.body.name,
      IsGlobal: req.body.IsGlobal
    },
    { new: true }
  );
  if (!permission) return res.status(404).send("the permission is not found");
  res.send(permission);
});

//4-delete
router.delete("/:id", async (req, res) => {
  const permission = await Permission.findOneAndRemove(req.params.id);
  if (!permission) return res.status(404).send("the permission is not found");
  res.send(permission);
});

//5-get by id
router.get("/:id", async (req, res) => {
  const permission = await Permission.findById(req.params.id);
  if (!permission) return res.status(404).send("the permission is not found");
  res.send(permission);
});
module.exports=router;