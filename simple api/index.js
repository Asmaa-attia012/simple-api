const mongoose = require("mongoose");
const joi = require("joi");
const express = require("express");
const app = express();
const cors = require('cors');
const bodyParser=require('body-parser');
const permissions = require("./routes/permissions");
const roles = require("./routes/roles");
const views = require("./routes/views");

// 1-connect to db
mongoose
  .connect("mongodb://interview:mFcibySNx9SdpVa@ds147440.mlab.com:47440/interview-task", {
    useNewUrlParser: true
  })
  .then(() => console.log("connected to db.."))
  .catch(error => console.log("failed to connect to db.."));

//Third-party MiddleWare
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({
    extended: true
}));
app.use(cors());

//routes middleWares
app.use(express.json());
app.use('/permissions', permissions);
app.use("/views", views);
app.use("/roles", roles);


//port used
const port = process.env.PORT || 3000;
app.listen(port, () => console.log(`listening to port ${port}`));
